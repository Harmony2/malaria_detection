# Malaria Detection

This kaggle dataset contains two kinds of cell images, parasitized by malaria and uninfected. As shown bellow, its a binary classification problem. Based on the images, i expect the model to use color ranges and some shapes to classificate.

<h2>Uninfected</h2>

![alt text](/photos/C100P61ThinF_IMG_20150918_145609_cell_91.png)
![alt text](/photos/C102P63ThinF_IMG_20150918_161826_cell_152.png)
![alt text](/photos/C125P86ThinF_IMG_20151004_101158_cell_16.png)

<h2>Infected</h2>

![alt text](/photos/C101P62ThinF_IMG_20150918_151006_cell_61.png)
![alt text](/photos/C101P62ThinF_IMG_20150918_151006_cell_62.png)
![alt text](/photos/C101P62ThinF_IMG_20150918_151239_cell_84.png)

In this project i will test and document some architectures and their results.

*  The dataset used on this project can be found on kaggle https://www.kaggle.com/iarunava/cell-images-for-detecting-malaria
*  Training data = 22046 (80% of dataset)
*  Validation data = 5512 (20% of dataset)

I'll split this project in some sections, which will contain a new model each. I'll transition from a shallow, simpler model to a deeper, with more kernels and complexer model, registering each change and their performance. The objective is to graph the efficiency of various types of models, and analyse the behaviour of the neural networks on datasets with relatively big datasets, like this one. All the models will be convolutional neural networks.

I wont use any kind of augument data, normalization or dropout layers on the beggining, and ill mantain the length of the epochs and the learning rate, so we can have a raw observation of the models behaviour and accuracy. In the later stages, in order to create the most efficient model i can, i may add those things to the neural net, depending on necessity.

# 1st_model

The first model will be very simple and shallow. Keep in mind that this problem is not very complex itself, and thus can be reasonably soled by simpler models, this is possible not only due to the simplicity of the problem, but also to the number of samples on the dataset.

<h2>Architecture</h2>

For the first model, im using one, besides the input layer, one convolutional layer. It has 32 3x3 kernels, and relu as activation function. The dense portion has 500 units and also uses relu as activation. Given the binary nature of the problem, sigmoid is used on the last layer, which has a single unit.

    model = models.Sequential()

    model.add(layers.Conv2D(32, (3,3), activation="relu", input_shape=(150,150,3)))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(32, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Flatten())

    model.add(layers.Dense(500, activation="relu"))

    model.add(layers.Dense(1, activation="sigmoid"))

    model.summary()
    model.compile(loss="binary_crossentropy", optimizer=optimizers.RMSprop(lr=1e-4), metrics=["acc"])

    history = model.fit_generator(train_generator, steps_per_epoch=173, epochs=25, validation_data=val_generator, validation_steps=44, verbose=2)

![alt text](photos/1st_model.png)
![alt text](photos/1st_pred.png)

<h2>Conclusions</h2>

We've got a relatively "good result", even with this very, very simple architecture. The first epoch took a long time to run, but everysingle epoch after that took about 30 seconds to complete. There is a clear overfitting. Around the fifth epoch our validation accuracy (in green) stopped improving, while the training accuracy (in blue) got as high as 99%. At the end, we scored a 91% validation accuracy, and the model was able to correctly predict our choosen image (uninfected).

# 2nd_model

<h2>Architecture</h2>

The second model has one single difference from the first one; The number of kernels on each convolutional layer. Instead of using 32 kernels, i'm using 64.

    model = models.Sequential()

    model.add(layers.Conv2D(64, (3,3), activation="relu", input_shape=(150,150,3)))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(64, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Flatten())

    model.add(layers.Dense(500, activation="relu"))

    model.add(layers.Dense(1, activation="sigmoid"))

    model.summary()
    model.compile(loss="binary_crossentropy", optimizer=optimizers.RMSprop(lr=1e-4), metrics=["acc"])

    history = model.fit_generator(train_generator, steps_per_epoch=173, epochs=25, validation_data=val_generator, validation_steps=44, verbose=2)

![alt text](photos/2nd_model.png)
![alt text](photos/2nd_pred.png)

<h2>Conclusions</h2>

This model, acts pretty much like the first one. Around the fifth epoch it starts to overfit. As shown with the blue line, training accuracy was better in this model, and got to 100% before epoch 20, while validation accuracy unconsistently danced around 90%, with some minimal spikes peaking at 91%. This means the model is able to abstract more patterns to make its classifications, but it is indeed pick up the wrong patterns to look at, therefore, the 9 ~ 10% overfitting.

# 3rd_model

<h2>Architecture</h2>

Our 3rd model will follow a similar structure, using 128 kernels instead of 64.

    model = models.Sequential()

    model.add(layers.Conv2D(128, (3,3), activation="relu", input_shape=(150,150,3)))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(128, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Flatten())

    model.add(layers.Dense(500, activation="relu"))

    model.add(layers.Dense(1, activation="sigmoid"))

    model.summary()
    model.compile(loss="binary_crossentropy", optimizer=optimizers.RMSprop(lr=1e-4), metrics=["acc"])

    history = model.fit_generator(train_generator, steps_per_epoch=173, epochs=25, validation_data=val_generator, validation_steps=44, verbose=2)

![alt text](photos/3rd_model.png)
![alt text](photos/3rd_pred.png)

<h2>Conclusions</h2>

The high kernel count on this model allows for a faster and more abstract pattern learning. Our training accuracy reaches 100% faster, with a more accentuated curve, while our validation accuracy shows a more stable learning, with an also faster overfitting, which stabilizes along the epochs.

This very shallow architecture, with one input and one convolutional layer, isn't very realiable (at least for this problem, and for problems with similar or higher complexity) given it has limited abstractions for pattern localization. The first layer consists of $`(32 | 64 | 128) * 3 * 3 * 3`$ kernels, turning it's detection pretty simple. But even with this simplitic, naive aproach to the problem, we were able to classify correctly our dummy image with all first three models, and got scores higher than 90% on validation accuracy without any augmented data or overfitting mitigation methods.

# 4th_model

<h2>Architecture</h2>

In this set of models, we'll be using, three convolutional layers. For the fourth model, i'm using 32 kernels for each of those layers. Apart from this, every single aspect of the neural networks still the same, even the number of epochs. I decided to change a bit the grid on the graphs, just so its easier to tell the datapoints.

    model = models.Sequential()

    model.add(layers.Conv2D(32, (3,3), activation="relu", input_shape=(150,150,3)))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(32, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(32, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Flatten())

    model.add(layers.Dense(500, activation="relu"))

    model.add(layers.Dense(1, activation="sigmoid"))

    model.summary()
    model.compile(loss="binary_crossentropy", optimizer=optimizers.RMSprop(lr=1e-4), metrics=["acc"])

    history = model.fit_generator(train_generator, steps_per_epoch=173, epochs=25, validation_data=val_generator, validation_steps=44, verbose=2)

![alt text](photos/4th_model.png)
![alt text](photos/4th_pred.png)


<h2>Conclusions</h2>

This kind of architecture shows immediate improvement. It takes a little longer to learn patterns, but shows good results. Our training accuracy got around 98%, splitting paths from the validation accuracy at epoch 6. Validation doesn't disapoint, tho, getting a 94% accuracy on a spike, and ending the train with 92%. Stability is not the greatest, but overfitting improved a bit. And, of course, it doesn't fail to provide us with a correct labeling of our test image. I don't actually expect any of our models to misslabel this, at this point.

# 5th_model 

<h2>Architecture</h2>

Using 64 kernels on each conv layer

    model = models.Sequential()

    model.add(layers.Conv2D(64, (3,3), activation="relu", input_shape=(150,150,3)))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(64, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(64, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Flatten())

    model.add(layers.Dense(500, activation="relu"))

    model.add(layers.Dense(1, activation="sigmoid"))

    model.summary()
    model.compile(loss="binary_crossentropy", optimizer=optimizers.RMSprop(lr=1e-4), metrics=["acc"])

    history = model.fit_generator(train_generator, steps_per_epoch=173, epochs=25, validation_data=val_generator, validation_steps=44, verbose=2)

![alt text](photos/5th_model.png)
![alt text](photos/5th_pred.png)

<h2>Conclusions</h2>

This model learns faster and abstracts better then the privious one. There was a little bit less overfitting, around 1% less at the end. The final validation accuracy was 94%, the higher score right now, while the validation accuracy got around 96 ~ 97. This neural net showed more stable results, all the kernels steadly adapted to the dataset, picking some significant and abstract patterns, even tho it also picked some bad and dataset-specific ones, resulting in this little overfitting. But this is a good result; There is not much overfitting (at this moment on training), and there is a relatively realiable accuracy, with less than 6% error rate.

#6th_model

<h2>Architecture</h2>

This time we're using 128 kernels for each convolutional layer.

    model = models.Sequential()

    model.add(layers.Conv2D(128, (3,3), activation="relu", input_shape=(150,150,3)))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(128, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(128, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Flatten())

    model.add(layers.Dense(500, activation="relu"))

    model.add(layers.Dense(1, activation="sigmoid"))

    model.summary()
    model.compile(loss="binary_crossentropy", optimizer=optimizers.RMSprop(lr=1e-4), metrics=["acc"])

    history = model.fit_generator(train_generator, steps_per_epoch=173, epochs=25, validation_data=val_generator, validation_steps=44, verbose=2)

![alt text](photos/6th_model.png)
![alt text](photos/6th_pred.png)

<h2>Conclusions</h2>

This model showed no improvement compared to the last one. It actually performed worse. The high number of kernels made for an easy overfitting. Both convnets got to a more stable training accuracy improvement on epoch 5, with not much improvement on validation since then, but our 5th model didn't adapt to training data so much, and had a more stable validation accuracy curve. At the end of the training, this model scored 93.8%.

#7th_model

<h2>Architecture</h2>

Now will add a new convolutional layer, and get back to 32  3*3 kernels.

    model = models.Sequential()

    model.add(layers.Conv2D(32, (3,3), activation="relu", input_shape=(150,150,3)))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(32, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(32, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(32, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Flatten())

    model.add(layers.Dense(500, activation="relu"))

    model.add(layers.Dense(1, activation="sigmoid"))

    model.summary()
    model.compile(loss="binary_crossentropy", optimizer=optimizers.RMSprop(lr=1e-4), metrics=["acc"])

    history = model.fit_generator(train_generator, steps_per_epoch=173, epochs=25, validation_data=val_generator, validation_steps=44, verbose=2)

![alt text](photos/7th_model.png)
![alt text](photos/7th_pred.png)

<h2>Conclusions</h2>

This architecture already shows a lot more stability. Unlike all other models we've trained so far, this one still improves on validation accuracy after the fifth epoch. There is not much overfitting at all. Even tho this model didn't actually get a much higher score than the last ones did, it achieves 96% training and 94% validation accuracy with stability. Just 2% of overfitting with no active methods for mittigation whatsoever! This architecture seems to be able to make more precise and efficient abstractions, having consistent usage of the convolutional layers and their kernels.

# 8th_model

<h2>Architecture</h2>

Still using the same king of architecture as the previous model, but with 64 kernels per layer instead of 32.

    model = models.Sequential()

    model.add(layers.Conv2D(64, (3,3), activation="relu", input_shape=(150,150,3)))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(64, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(64, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(64, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Flatten())

    model.add(layers.Dense(500, activation="relu"))

    model.add(layers.Dense(1, activation="sigmoid"))

    model.summary()
    model.compile(loss="binary_crossentropy", optimizer=optimizers.RMSprop(lr=1e-4), metrics=["acc"])

    history = model.fit_generator(train_generator, steps_per_epoch=173, epochs=25, validation_data=val_generator, validation_steps=44, verbose=2)

![alt text](photos/8th_model.png)
![alt text](photos/8th_pred.png)


<h2>Conclusions</h2>

This model seems to be our best so far. It starts the training doing very well right away, getting to +93% on the second epoch. The convolutional network mantains the previous model's stability, with some actual improvement to this aspect.

There is an ongoing, slow and steady improvement happening on training accuracy, and also (but a bit less) on validation too. This means the network is still tunning its patterns and abstractions; There is still room for improvement. The model is the fastest and most stable so far, with very little overfitting, just like the previous one.

# 9th_model

<h2>Architecture</h2>

Same thing, but with double the kernels.

    model = models.Sequential()

    model.add(layers.Conv2D(128, (3,3), activation="relu", input_shape=(150,150,3)))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(128, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(128, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(128, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Flatten())

    model.add(layers.Dense(500, activation="relu"))

    model.add(layers.Dense(1, activation="sigmoid"))

    model.summary()
    model.compile(loss="binary_crossentropy", optimizer=optimizers.RMSprop(lr=1e-4), metrics=["acc"])

    history = model.fit_generator(train_generator, steps_per_epoch=173, epochs=25, validation_data=val_generator, validation_steps=44, verbose=2)

![alt text](photos/9th_model.png)
![alt text](photos/9th_pred.png)

<h2>Conclusions</h2>

Well. Overfitting. Disappoiting. Too many kernels.

#10_model

Now, using what we've observated, i'll try to build a more complex model, with what i think will be efficient to achieve our higher possible score.

<h2>Architecture</h2>

So, this is supposed to be our best possible model for this specific problem, using every information we've collected so far. After observating our other models, we can see a certain pattern on their performances. Some performe better than the others, some overfit less, some are more stable, some are better at making abstractions. For this convnet, i've picked our most stable, highest score model, the 8th one.

The four convolutional layers allow for specific and important kinds of abstraction on each one of them, splitting the responsability for each kind of important aspect of the images; One layer looks for one particular pattern of shapes, while other, deeper, layers look for color ranges, for exemple. The first 3 layers have 64 kernels, wich showed a more stable learning patter on our tests, but i've used 128 on our last convolutional layer, for more decisive abstractions, and faster pattern recognition. I've added 10% dropout to this layer, to prevent overfitting.

For our dense block, i've used a single layer, composed of 1000 units and 25% dropout, also to prevent overfitting. The "high" units count is suposed to prevent information from bottlenecking at units. Maybe i should've added one more layer to our dense block, and with double the dropout to both layers, as this could give it more room to learn about the data properly.

Also, i decided not to content myself with only 22K datasamples. If i had 22K, why not shove 66k into this beautiful little neural network? So i've used data augmentation.

    train_datagen = ImageDataGenerator(rescale=1./255, rotation_range=180, fill_mode='nearest', horizontal_flip=True, vertical_flip=True)
    val_datagen = ImageDataGenerator(rescale=1./255)

I've trained the model for 689 steps per epoch, 20 epochs. $`lr = 1e-^4`$

    model = models.Sequential()

    model.add(layers.Conv2D(64, (3,3), activation="relu", input_shape=(150,150,3)))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(64, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(64, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))

    model.add(layers.Conv2D(128, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D(2,2))
    model.add(layers.Dropout(0.1))

    model.add(layers.Flatten())

    model.add(layers.Dense(1000, activation="relu"))
    model.add(layers.Dropout(0.25))

    model.add(layers.Dense(1, activation="sigmoid"))

    model.summary()
    model.compile(loss="binary_crossentropy", optimizer=optimizers.RMSprop(lr=1e-4), metrics=["acc"])

    history = model.fit_generator(train_generator, steps_per_epoch=689, epochs=20, validation_data=val_generator, validation_steps=44, verbose=2)

![alt text](photos/10th_0_model.png)

I know, just looking at it it doesn't look like it was that great, but this first graph has $`ymin = 0.865`$ $`ymax = 0.97`$.

Lets have a look at $`ymin = 0.6`$ $`ymax = 1`$ and compare it with our 8th model, which had the best results so far.

![alt text](photos/8th_model.png)
![alt text](photos/10th_01_model.png)

This model had very low overfitting rate, peacking at < 2% at epoch 15. It also had the best validation scores so far, reaching a never seen 95.12% peak at epoch 19.
Training took around 361 s/epoch, resulting in 2 hours training.

With these weights, i have now some options;

  - Retrain the neural net from 0, fine tunning some values
  - Continue to train this network with new augmented data, changing some hyper parameters
  - Import the convolutional layers into a new model, and train it with augmented data

I don't actually want to retrain this network from 0, i don't see much advantages, specially for the time needed to retrain it. Both other options seem like good ideas, so i decided to check the consistency of our convolutional layers before commiting to any one of them; If they're doing fine, i'll import them into a new model, and freeze them, making for a hopefully and helpful faster training. 

    train_datagen = ImageDataGenerator(rescale=1./255)
    val_datagen = ImageDataGenerator(rescale=1./255)

    train_generator = train_datagen.flow_from_directory("./train/", target_size=(150,150), batch_size=128, class_mode="binary")
    val_generator = val_datagen.flow_from_directory("./val/", target_size=(150,150), batch_size=128, class_mode="binary")

    model = models.load_model("./models/10th_model.h5")

    for layer in model.layers[:9]:
        layer.trainable = False

    for i in range(0,3):
        model.pop()
        
    for layer in model.layers:
        print(layer, layer.trainable)

    model.add(layers.Dense(1000, activation="relu"))
    model.add(layers.Dropout(0.2))
    model.add(layers.Dense(500, activation="relu"))
    model.add(layers.Dropout(0.1))
    model.add(layers.Dense(1, activation="sigmoid"))
    model.summary()

    model.compile(loss="binary_crossentropy", optimizer=optimizers.RMSprop(lr=1e-4), metrics=["acc"])

    history = model.fit_generator(train_generator, steps_per_epoch=173, epochs=5, validation_data=val_generator, validation_steps=44, verbose=1)

I this case, i wanted to import the model and extract features with the convnet to a new densenet, and then make some new tests on it, but i ended up having a problem with the process. So, instead, i've imported the model, and froze the convolutional base; Then removed the dense block of the model and added new layers; Two of them, with 1000 units and 20% dropout, and 500 units and 10% dropout each. I've trained the model for a few epochs (around 6 or 7) to consolidate the validation accuracy at 95%, with 97% training accuracy. > 2% overfitting, and a nice accuracy!

<h2>Conclusions</h2>